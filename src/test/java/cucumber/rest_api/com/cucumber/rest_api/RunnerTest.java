package cucumber.rest_api.com.cucumber.rest_api;
/**
 * Created by reiload on 12/12/2015.
 */
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(

        // caminho da feature
        features = "src/test/resource",

        // no plugin vai gerar o relatorio, em html e json para integracao continua.
        plugin = { "pretty",
                "html:target/site/cucumber-pretty",
                "json:target/cucumber.json" } )

public class RunnerTest {

}
