package cucumber.rest_api.com.cucumber.rest_api;

public class xml {
	protected static int code_status;
    protected static int code;
    protected static String publicKey;
    protected static String mollom_site = null;
    
	public static String getPublicKey() {
		return publicKey;
	}
	public static void setPublicKey(String publicKey) {
		xml.publicKey = publicKey;
	}
	public static int getCode() {
		return code;
	}
	public static void setCode(int code) {
		xml.code = code;
	}
	public static int getCode_status() {
		return code_status;
	}
	public static void setCode_status(int code_status) {
		xml.code_status = code_status;
	}
	public static String getMollom_site() {
		return mollom_site;
	}
	public static void setMollom_site(String mollom_site) {
		xml.mollom_site = mollom_site;
	}
}
