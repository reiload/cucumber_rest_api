package cucumber.rest_api.com.cucumber.rest_api;

import java.io.*;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Properties;

public class functions {


    public String getConfig(String name)  {

        InputStream inputStream = null;
        String value = "";
        try {
            Properties prop = new Properties();
            String propFileName = "conf.properties";

            inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

            if (inputStream != null) {
                prop.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
            }

            // get the property value and print it out
            value = prop.getProperty(name);
            //System.out.println(value);
            return value;

        } catch (Exception e) {

            System.out.println("Exception: " + e);
            return value;
        }

    }

    public void setPropertiesKey(String publicKey ) throws IOException, URISyntaxException {

        InputStream inputStream = null;
        Properties prop = new Properties();
        String propFileName = "conf.properties";

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

        if (inputStream != null) {
            prop.load(inputStream);
            inputStream.close();
        } else {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }

        prop.setProperty("publicKey", publicKey);
        URL url = getClass().getClassLoader().getResource(propFileName);
        prop.store(new FileOutputStream(new File(url.toURI())), null);

    }
    public void setPropertiesSite(String site ) throws IOException, URISyntaxException {

        InputStream inputStream = null;
        Properties prop = new Properties();
        String propFileName = "conf.properties";

        inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);

        if (inputStream != null) {
            prop.load(inputStream);
            inputStream.close();
        } else {
            throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
        }

        prop.setProperty("mollom_site", site);
        URL url = getClass().getClassLoader().getResource(propFileName);
        prop.store(new FileOutputStream(new File(url.toURI())), null);
        System.out.println("Novo site adicionado com sucesso!");

    }

}
