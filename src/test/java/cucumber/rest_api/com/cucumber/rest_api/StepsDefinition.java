package cucumber.rest_api.com.cucumber.rest_api;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;

public class StepsDefinition {


        Rest_Assured rest_assured = new Rest_Assured();
        String newsite;
        int update_url;
        int update_email;
        functions conf = new functions();

    @Given("^Dado um \"([^\"]*)\" para as chamadas dessa API$")
        public void dado_um_para_as_chamadas_dessa_API(String site) throws Throwable {

            // Seta o site no arquivo properties
            // nesse caso todos os testes podem utilizar o site, sem passar por parametro.
            conf.setPropertiesSite(site);

        }
    @When("^Criar um novo site definindo uma \"([^\"]*)\" e um \"([^\"]*)\" para o mesmo$")

        public void criar_um_novo_site_definindo_uma_e_um_para_o_mesmo(String url, String email) throws Throwable {

            newsite = rest_assured.createSite("?url=" +url + "&email=" + email);

        }

    @Then("^Validar que foi criado com sucesso$")
        public void validar_que_foi_criado_com_sucesso() throws Throwable {

            int site = rest_assured.postRequest(newsite,"");
            Assert.assertEquals(200, site);

        }

    @Given("^Fazer um update somente da \"([^\"]*)\" desse site$")
        public void fazer_um_update_somente_da_desse_site(String url) throws Throwable {
            String newsite = conf.getConfig("publicKey");
            update_url = rest_assured.postRequest(newsite,
                    "?url="+url);
        }

    @Given("^Fazer um update somente do \"([^\"]*)\" desse site$")
        public void fazer_um_update_somente_do_desse_site(String email) throws Throwable {
            String newsite = conf.getConfig("publicKey");
            update_email = rest_assured.postRequest(newsite,
                    "?email="+email);
        }

    @Then("^validar que foi feito com sucesso$")
        public void validar_que_foi_feito_com_sucesso() throws Throwable {
    		System.out.println("Valida a URL: " + update_url);
            Assert.assertEquals(200, update_url);
    		System.out.println("Valida o Email: " + update_email);
            Assert.assertEquals(200, update_email);
        }


}
