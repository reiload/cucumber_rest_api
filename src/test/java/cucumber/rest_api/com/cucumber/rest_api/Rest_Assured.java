package cucumber.rest_api.com.cucumber.rest_api;

import com.jayway.restassured.path.xml.XmlPath;
import static com.jayway.restassured.RestAssured.*;

public class Rest_Assured extends xml{

    static functions conf = new functions();

    public int postRequest(String url, String parameters) throws Exception {
        // caso o der erro na funcao vai retorna o erro 500 error no servidor.
        code = 500;
        try{
            mollom_site = conf.getConfig("mollom_site");
            // faz o Post de acordo com os parametros passados.
            String url_path = mollom_site + "/" + url + parameters;
            System.out.println("path: " + url_path+"\n");
            // obtem o response do request realizado.
            final String xml  = post(url_path).
                    andReturn().asString();
            System.out.println("Passa a string e pega o codigo do xml: " + xml);
            // pegamos o codigo do response, a funcao converte pra xml a string
            // e pega o valor que desejo.
            code = getCode(xml);
            return code;

        }catch(Exception e){
            System.out.println("Site ja Nao Exite!"+ e);
            return code;
        }
    }

    public String createSite(String parameters) throws Exception {

        mollom_site = conf.getConfig("mollom_site");
        // faz o Post de acordo com os parametros passados.
        final String xml  = post(mollom_site + parameters).
                andReturn().asString();
        publicKey = getXML_publicKey(xml);
        conf.setPropertiesKey(publicKey);
        return publicKey;
    }

    public static String getXML_publicKey(String xml){

        // Passa a String para XML
        XmlPath xmlPath = new XmlPath(xml).setRoot("siteResponse");
        // Pega os parametros do XML.
        System.out.println(xmlPath);
        String code = xmlPath.get("code");
        String publicKey = xmlPath.get("site.publicKey");
        String privateKey = xmlPath.get("site.privateKey");
        // Imprime os resultados.
        System.out.println("\nCreate site status: " + code);
        System.out.println(publicKey);
        System.out.println(privateKey);
        // Retorna o valor da public key.
        return publicKey;
    }

    public static int deleteSite(String publicKey) throws Exception {

        code = 500;
        try{
            mollom_site = conf.getConfig("mollom_site");
            // faz o Post de acordo com os parametros passados.

            final String xml  = post(mollom_site + "/" + publicKey + "/?delete").
                    andReturn().asString();
            // Pega o code do XML.
            code = getCode(xml);
            // Imprime os resultados.
            System.out.println("\nDelete site status: " + code);
            return code;
        }
        catch(Exception e){
            System.out.println("Site ja Nao Exite!"+ code);
            return code;
        }
    }
    public static int getCode(String xml){

        // Passa a String para XML
        XmlPath xmlPath = new XmlPath(xml).setRoot("siteResponse");
        System.out.println(xmlPath);
        // Pega os parametros do XML.
        String code = xmlPath.get("code");
        String publicKey = xmlPath.get("site.publicKey");
        String privateKey = xmlPath.get("site.privateKey");
        // Imprime os resultados.
        System.out.println("\nCode: " + code);
        System.out.println("Public Key: "+publicKey);
        System.out.println("Private Key: "+privateKey);
        code_status = Integer.parseInt(code);
        return code_status;
    }
}
