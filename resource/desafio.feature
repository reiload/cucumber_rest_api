Feature: Dado uma API pública -> Mollom API e sua documentação para validacao

  Scenario Outline: Criar um novo site
    Given Dado um "<Site>" para as chamadas dessa API
    When Criar um novo site definindo uma "<URL>" e um "<Email>" para o mesmo
    Then Validar que foi criado com sucesso

    Examples: 
      | Site                          | URL                    | Email             |
      | http://dev.mollom.com/v1/site | www.vidadetestador.com | reiload@gmail.com |

  Scenario Outline: Fazer update da url e do email no novo site
    Given Fazer um update somente da "<URL_Update>" desse site
    And Fazer um update somente do "<Email_Update>" desse site
    Then validar que foi feito com sucesso

    Examples: 
      | URL_Update               | Email_Update                  |
      | www.vidadetestadores.com | reinaldo.rossetti@outlook.com |
